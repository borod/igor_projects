﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketProfile.DataStructures
{
  class Parameters
  {
    public DateTimeOffset DateFrom { get; set; }
    public DateTimeOffset DateTo { get; set; }
    public DateTimeOffset KindOfBars { get; set; }
    public DateTimeOffset Symbol { get; set; }
    public DateTimeOffset PercentOfValuearea { get; set; }
  }
}
