﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarketProfile.DataStructures
{
  class Output
  {
    public double POC { get; set; }
    public double ValueAreaHigh { get; set; }
    public double ValueAreaLow { get; set; }
  }
}
